import UtilisateurCourant from './Memoire/UtilisateurCourant'


// Utilisateur Courant
// pour l'ensemble des tests qui suivent
UtilisateurCourant.update("Bob")


it('Test update UtilisateurCourant Nom', () => {
  expect(UtilisateurCourant.getNom()).toBe("Bob")
})


it('Test init UtilisateurCourant Messages', () => {
  expect(UtilisateurCourant.getMessages()).toEqual([])
})


it('Test setMessages UtilisateurCourant', () => {
  UtilisateurCourant.setMessages([{id : 1 , message :"Test set", like : 5}])
  expect(UtilisateurCourant.getMessages()).toEqual([{id : 1 , message :"Test set", like : 5}])
})

it('Test ajouterListeMessage UtilisateurCourant', () => {
  UtilisateurCourant.ajouterListeMessage([{_id : 2, message :"Message 1", like :1},{_id : 3, message :"Message 2", like :1}, {_id : 4, message :"Message 3", like :5}])
  expect(UtilisateurCourant.getMessages()).toEqual([{id : 1 , message :"Test set", like : 5},{id : 2, message :"Message 1", like :1},{id : 3, message :"Message 2", like :1}, {id : 4, message :"Message 3", like :5}])
})


it("Test clearMessage UtilisateurCourant", () => {
  UtilisateurCourant.clearMessages()
    expect(UtilisateurCourant.getMessages()).toEqual([])
})

it("Test clear UtilisateurCourant Messages", () => {
  UtilisateurCourant.ajouterListeMessage([{message :"Message 1"},{message :"Message 2"}, {message :"Message 3"}])
  UtilisateurCourant.clear()
  expect(UtilisateurCourant.getMessages()).toEqual([])
})

it("Test clear UtilisateurCourant Messages", () => {
  UtilisateurCourant.update("Bob")
  UtilisateurCourant.clear()
  expect(UtilisateurCourant.getNom()).toBe("")
})
