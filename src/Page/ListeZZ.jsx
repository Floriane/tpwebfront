import React, { Component } from 'react'
import {Preloader} from 'react-materialize'
import axios from 'axios'

import BarreNavigation from "../Component/BarreNavigation"
import ZZ from "../Component/ZZ"

import Utilisateurs from "../Memoire/Utilisateurs"

class ListeZZ extends Component {

  constructor(props)
  {
      super(props)
      this.state = {chargement : true}
  }

  componentDidMount()
  {
      Utilisateurs.clear()
      axios.post('http://localhost:5001/api/v1/users', {page : 1, nombrePost : 5}, {headers: {authorization : localStorage.getItem("token"), "Content-Type": "application/json"}})
            .then(retour =>{
              Utilisateurs.setUtilisateurs(retour.data.listeUtilisateurs)
              this.setState({chargement : false})
            })
            .catch(err => console.log("Impossible de charger les utilisateurs"))
  }


  render() {
    return (
      <div>
        <BarreNavigation history={this.props.history}/>
        <div class="body" style={bodyStyle}>
        {
          this.state.chargement ?
          (
              <Preloader size='big'/>
          ) :
          (
              <div style={listeStyle}>
                { Utilisateurs.getUtilisateurs().map((utilisateur) => (<ZZ utilisateur={utilisateur} history={this.props.history}/>))}
              </div>
          )
        }
        </div>
      </div>
    );
  }
}


const bodyStyle = {
  display : "flex",
  alignItems : "center",
  justifyContent : "center",
};

const listeStyle = {
  margin:"0",
  padding:"0",
  display : "flex",
  alignItems : "center",
  flexWrap: "wrap",
  justifyContent : "center",
};

export default ListeZZ;
