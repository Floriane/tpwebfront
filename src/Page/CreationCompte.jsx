import React, { Component } from 'react'
import {Button, Icon, Input} from 'react-materialize'
import axios from 'axios'

import TitreInstaZZ from '../Component/TitreInstaZZ'
import Image from '../Graphic/Picture/appareilPhoto.jpg'

import '../CSS/Input.css'

class CreationCompte extends Component {

  constructor(props)
  {
      super(props);
        this.state = {identifiant : "", motDePasse : "", motDePasseConf : "", erreurChampVide : "", erreurConfirmation : "", erreurNom: ""}
  }

  verification = () =>
  {
    var continuer = true
    if( (this.state.identifiant === "") || (this.state.motDePasse === "")  || (this.state.motDePasseConf === ""))
    {
        this.setState({erreurChampVide: "Un des champs est vide.", erreurConfirmation: "",erreurNom: "" })
        continuer = false
    }
    if( this.state.motDePasse !== this.state.motDePasseConf)
    {
      this.setState({erreurChampVide: "" , erreurConfirmation: "Les mots de passes sont différents.", erreurNom:"" })
      continuer = false
    }

    if(continuer)
    {
      axios.post('http://localhost:5001/api/v1/nouveauUtilisateur', {nom: this.state.identifiant, motDePasse: this.state.motDePasse })
            .then(res =>{
              this.props.history.push('/connexion')
            })
            .catch(err => this.setState({erreurChampVide: "", erreurConfirmation: "", erreurNom : "L'identifiant est déjà utilisé"}))
    }

  }


  render() {
    return (
      <div style={globalStyle}>
        <div  className="white z-depth-5" style={englobantStyle}>
          <TitreInstaZZ/>
          <div id="erreurChampVide" style={erreurStyle}>{this.state.erreurChampVide} </div>
          <div id="erreurConfirmation" style={erreurStyle}>{this.state.erreurConfirmation} </div>
          <div id="erreurIdentifiant" style={erreurStyle}>{this.state.erreurNom} </div>
          <Input className="blue-is-active"placeholder="Identifiant" value={this.state.identifiant}  onChange={saisie => this.setState({identifiant: saisie.target.value.trim()})}><Icon className="material-icons yellow">account_circle</Icon></Input>
          <Input type="password" placeholder="Mot de passe" value={this.state.motDePasse}  onChange={saisie => this.setState({motDePasse: saisie.target.value})}><Icon>lock</Icon></Input>
          <Input type="password" placeholder="Mot de passe" value={this.state.motDePasseConf}  onChange={saisie => this.setState({motDePasseConf: saisie.target.value})}><Icon>lock</Icon></Input>
          <Button className="light-blue darken-4"id="validationCreation" style={{marginTop:"10%"}} onClick={this.verification}>Valider</Button>
        </div>
      </div>
    );
  }
}


const globalStyle = {
  margin:"0",
  padding:"0",
  backgroundImage: "url(" + Image + ")",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  height : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
  WebkitBackgroundSize: "cover",
  display : "flex",
  justifyContent : "center",
  alignItems : "center",
}

const englobantStyle = {
  paddingBottom : "50px",
  paddingLeft : "50px",
  paddingRight : "50px",
  display : "flex",
  flexDirection : "column",
  alignItems : "center",
}

const erreurStyle = {
  color : "red",
  fontSize : "15px",
}


export default CreationCompte
