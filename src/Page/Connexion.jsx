import React, { Component } from 'react'
import {Button, Input} from 'react-materialize'
import axios from 'axios'

import TitreInstaZZ from '../Component/TitreInstaZZ'
import Image from '../Graphic/Picture/appareilPhoto.jpg'

import UtilisateurConnecte from "../Memoire/UtilisateurConnecte"

import '../CSS/Input.css'

class Connexion extends Component {

  constructor(props)
  {
    super(props)
    this.state = {identifiant : "", motDePasse : "", erreur : "",  erreurSuite:"" }
  }

  valider = () =>
  {
    axios.post('http://localhost:5001/api/v1/identifierUser',{nom: this.state.identifiant, motDePasse: this.state.motDePasse})
          .then(res =>{
            UtilisateurConnecte.init(this.state.identifiant)
            console.log(res)
            localStorage.setItem('token', res.data.token)
            localStorage.setItem('utilisateur', this.state.identifiant)
            this.props.history.push('/mur')
          })
          .catch(err => this.setState({erreur : "L'identifiant ou le mot de passe", erreurSuite:" est incorrecte."}))
  }


  render() {
    return (
      <div style={globalStyle}>
        <div className="white z-depth-5" style={englobantStyle}>
          <TitreInstaZZ/>
          <div id="message" style={erreurStyle}>{this.state.erreur} </div>
          <div id="messageSuite" style={erreurStyle} >{this.state.erreurSuite} </div>
          <Input placeholder="Identifiant" value={this.state.identifiant}  onChange={saisie => this.setState({identifiant: saisie.target.value})}/>
          <Input type="password" placeholder="Mot de passe" value={this.state.motDePasse}  onChange={saisie => this.setState({motDePasse: saisie.target.value})}  />
          <Button className="light-blue darken-4"id="validation" style={{marginTop:"10%"}} onClick={this.valider}>Valider</Button>
        </div>
      </div>
    );
  }
}

const globalStyle = {
  margin:"0",
  padding:"0",
  backgroundImage: "url(" + Image + ")",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  height : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
  WebkitBackgroundSize: "cover",
  display : "flex",
  justifyContent : "center",
  alignItems : "center",
}

const englobantStyle = {
  paddingBottom : "50px",
  display : "flex",
  paddingLeft : "50px",
  paddingRight : "50px",
  flexDirection : "column",
  alignItems : "center",
}

const erreurStyle = {
  color : "red",
  fontSize : "15px",
}

export default Connexion
