import React, { Component } from 'react';
import {Button,CollapsibleItem,Collapsible,Input, Preloader} from 'react-materialize';
import axios from 'axios'

import BarreNavigation from "../Component/BarreNavigation";
import ListeMessages from "../Component/ListeMessages";

import UtilisateurConnecte from "../Memoire/UtilisateurConnecte"
import UtilisateurCourant from "../Memoire/UtilisateurCourant"

import '../CSS/Input.css';
import '../CSS/Collapsible.css'

class Mur extends Component {

  constructor(props)
  {
      super(props);
      this.state = {nouveauMessage: "", chargement: true , pageCourante: 1, rechargerPage : false, pictures : [], messageImage :"Choisir une image"};
      this.proprietaireMur = UtilisateurConnecte ;
  }

  componentDidMount()
  {
    this.proprietaireMur.clearMessages()
    this.chargerNouveauMessage()
  }


  initProprietaire = () =>
  {
    if (!UtilisateurConnecte.getEstRegarde())
    {
      this.proprietaireMur = UtilisateurCourant
    }
    else
    {
      this.proprietaireMur = UtilisateurConnecte
    }
    localStorage.setItem("regarde", this.proprietaireMur.getNom())
  }

  valider = () =>
  {
    if( this.state.nouveauMessage !== "")
    {
      axios.post('http://localhost:5001/api/v1/ajouterPost', {auteur : UtilisateurConnecte.getNom(), message:  this.state.nouveauMessage } , {headers: {authorization : localStorage.getItem("token"), "Content-Type": "application/json"}})
            .then(retour =>{
              UtilisateurConnecte.ajouterMessage(retour.data.nouveauPost);
              this.setState({nouveauMessage: ""});
            })
            .catch(err => console.log("Impossible de charger les messages"))
    }
  }

  chargerNouveauMessage = () =>
  {
      axios.post('http://localhost:5001/api/v1/posts', {page : this.state.pageCourante, nombrePost : 5, auteur : this.proprietaireMur.getNom() }, {headers: {authorization : localStorage.getItem("token"), "Content-Type": "application/json"}})
            .then(retour =>{
              this.proprietaireMur.ajouterListeMessage(retour.data.listePosts);
              this.setState({chargement : false, pageCourante : this.state.pageCourante + 1 })
            })
            .catch(err => console.log("Impossible de charger les messages"))
  }

  nomUtilisateur = () =>
  {
    return this.proprietaireMur.getNom()
  }

  annuler = () =>
  {
    this.setState({nouveauMessage: ""});
  }

  getMessages = () =>
  {
    return this.proprietaireMur.getMessages()
  }

  rechargerPage = () =>
  {
    console.log("MUR")
    this.setState({rechargerPage : !this.state.rechargerPage})
  }

  render() {
    this.initProprietaire()
    return (
      <div>
        <BarreNavigation history={this.props.history}/>
        <div class="body" style={bodyStyle}>
          <div className="light-blue-text text-darken-4" style={nameStyle}>{this.nomUtilisateur()}</div>
          {
            UtilisateurConnecte.getEstRegarde() ?
            (
              <Collapsible style={collapsibleStyle}>
                <CollapsibleItem header={(<div className='light-blue darken-4 white-text'>Ajouter un message</div>)}  icon='add_circle'>
                  <div style={formStyle}>
                    <Input type='textarea' value={this.state.nouveauMessage}  onChange={saisie => this.setState({nouveauMessage: saisie.target.value})}/>
                    <div style={buttonsStyle}>
                      <Button className='light-blue darken-4 white-text' style={buttonStyle} onClick={this.annuler}>Annuler</Button>
                      <Button className='light-blue darken-4 white-text' style={buttonStyle} onClick={this.valider}>Valider</Button>
                    </div>
                  </div>
                </CollapsibleItem>
              </Collapsible>
            ) :
            ( <div/>)
          }

          {
            this.state.chargement ?
            (
                <Preloader size='big'/>
            ) :
            (
                <ListeMessages listeMessages={this.getMessages()} rechargement={this.rechargerPage}/>
            )
          }
          <Button className="light-blue darken-4 white-text" onClick={this.chargerNouveauMessage}>PLUS DE MEZZAGES</Button>
        </div>
      </div>
    );
  }
}

const formStyle = {
  display: "flex",
  flexDirection: "column",
}

const buttonsStyle = {
  display: "flex",
  justifyContent : "flex-end",
}

const buttonStyle = {
  marginLeft : "5px",
}

const bodyStyle = {
  margin:"0",
  padding:"0",
  display : "flex",
  flexDirection : "column",
  alignItems : "center",
}

const nameStyle = {
  fontSize : "50px",
  marginBottom : "1%",
}

const collapsibleStyle={
  width : "80%",
  marginBottom : "1%",
}

export default Mur;
