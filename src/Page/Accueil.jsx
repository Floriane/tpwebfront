import React, { Component } from 'react'
import axios from 'axios'
import {Button} from 'react-materialize'

import Image from '../Graphic/Picture/appareilPhoto.jpg'
import TitreInstaZZ from '../Component/TitreInstaZZ'

class Accueil extends Component {

  /* test de l'API*/
  getUser = () =>
  {
    axios.get('http://localhost:5001/api/v1/users')
          .then(res =>console.log(res))
          .catch(err => console.error(err))
  }

  creerCompte = () =>
  {
    this.props.history.push('/creationCompte')
  }

  seConnecter = () =>
  {
    this.props.history.push('/connexion')
  }

  seDeplacer = () =>
  {
    this.props.history.push('/mur')
  }

  render() {
    return (
      <div style={globalStyle}>
        <div  style={englobantStyle}>
          <TitreInstaZZ/>
          <div id="buttons" style={buttonsStyle}>
           <Button className="light-blue darken-4" id="seConnecter" onClick={this.seConnecter}>Se connecter</Button>
           <Button className="light-blue darken-4 black-text" id="creerCompte" onClick={this.creerCompte}>Créer un compte</Button>
           <Button className="light-blue darken-4 black-text" id="informations"  waves='light'>Pourquoi InstaZZ ?</Button>
          </div>

        </div>
      </div>
    );
  }
}

const globalStyle = {
  margin:"0",
  padding:"0",
  backgroundImage: "url(" + Image + ")",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
  height : window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
  WebkitBackgroundSize: "cover",
  display : "flex",
  justifyContent : "center",
  alignItems : "center",
}

const englobantStyle = {
  height : "60%",
  display : "flex",
  flexDirection : "column",
  alignItems : "center",
}

const buttonsStyle = {
  height : "50%",
  marginTop : '30%',
  display : "flex",
  flexDirection : "column",
  alignItems : "center",
  justifyContent : "space-between",
}




export default Accueil
