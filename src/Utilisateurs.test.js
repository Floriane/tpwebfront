import Utilisateurs from './Memoire/Utilisateurs'

it('Test constructor Utilisateurs', () => {
  expect(Utilisateurs.getUtilisateurs()).toEqual([])
})

it('Test setUtilisateurs Utilisateurs', () => {
  Utilisateurs.setUtilisateurs([{nom: "Bob"}, {nom: "Lisa"}])
  expect(Utilisateurs.getUtilisateurs()).toEqual(["Bob", "Lisa"])
})

it('Test clear Utilisateurs', () => {
  Utilisateurs.clear()
  expect(Utilisateurs.getUtilisateurs()).toEqual([])
})
