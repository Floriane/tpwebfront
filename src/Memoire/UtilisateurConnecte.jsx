class UtilisateurConnecte {

    static nom = ""
    static messages = []
    static estRegarde = true

    static init(nomUtilisateur)
    {
      this.messages = []
      //remplissage avec les infos du serveur
      this.nom = nomUtilisateur
      this.estRegarde = true
    }

    static getNom()
    {
      return this.nom
    }

    static getMessages()
    {
      return this.messages
    }

    static ajouterMessage(message) // Ajouter appel API
    {
      this.messages.unshift({message : message.message, like : 0, id : message._id })
      if(this.messages.length > 5)
      {
        this.messages.pop()
      }
    }

    static ajouterListeMessage(listePosts)
    {
      var i
      for(i = 0; i < listePosts.length; i++)
      {
        this.messages.push({message : listePosts[i].message, like : listePosts[i].like, id : listePosts[i]._id })
      }
    }

    static supprimerMessage(id)
    {
      var i = 0
      while(i < this.messages.length && this.messages[i].id !== id)
      {
        i ++
      }
      if(i < this.messages.length)
      {
        this.messages.splice(i,1)
      }
      console.log(this.messages)

    }

    static clearMessages()
    {
      this.messages = []
    }

    static clear()
    {
      this.messages = []
      this.nom = ""
    }

    static setEstRegarde(nouveauEtat)
    {
      this.estRegarde = nouveauEtat
    }

    static getEstRegarde()
    {
      return this.estRegarde
    }


}

export default UtilisateurConnecte
