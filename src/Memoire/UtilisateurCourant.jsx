class UtilisateurCourant {

    static nom = ""
    static messages = []

    static update(nom)
    {
      this.nom = nom
      this.messages = []
    }

    static getNom()
    {
      return this.nom
    }

    static getMessages()
    {
      return this.messages
    }

    static setMessages(messages)
    {
      this.messages = messages
    }

    static clearMessages()
    {
      this.messages = []
    }

    static ajouterListeMessage(listePosts)
    {
      var i
      for(i = 0; i < listePosts.length; i++)
      {
        this.messages.push({message : listePosts[i].message, like : listePosts[i].like, id : listePosts[i]._id} )
      }
    }

    static clear()
    {
      this.nom = ""
      this.messages = []
    }


}

export default UtilisateurCourant
