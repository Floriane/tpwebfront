class Utilisateurs {

    static utilisateurs = []

    static clear()
    {
      this.utilisateurs = []
    }

    static getUtilisateurs()
    {
      return this.utilisateurs
    }

    static setUtilisateurs(listeUtilisateurs)
    {
      this.utilisateurs = []
      var i
      for(i = 0; i < listeUtilisateurs.length; i++)
      {
        this.utilisateurs.push(listeUtilisateurs[i].nom)
      }
    }


}

export default Utilisateurs
