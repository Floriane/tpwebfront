import React, { Component } from 'react';
import {Route} from 'react-router-dom'
import './App.css';
import Accueil from './Page/Accueil';
import CreationCompte from './Page/CreationCompte';
import Connexion from './Page/Connexion';
import ListeZZ from './Page/ListeZZ';
import Mur  from './Page/Mur';
import UtilisateurConnecte from "./Memoire/UtilisateurConnecte"
import UtilisateurCourant from "./Memoire/UtilisateurCourant"

class App extends Component {

  componentWillMount()
  {
    UtilisateurConnecte.init(localStorage.getItem("utilisateur"))
    if(localStorage.getItem("utilisateur") !== localStorage.getItem("regarde"))
    {
      UtilisateurCourant.update(localStorage.getItem("regarde"))
      UtilisateurConnecte.setEstRegarde(false)
    }
  }

  render() {
    let estConnecte = (UtilisateurConnecte.getNom() !== "")
    console.log(estConnecte)
    return (
      <div>
        <div className="App-intro">
          <Route exact path="/creationCompte" component={CreationCompte}/>
          <Route exact path="/connexion" component={Connexion}/>
          {estConnecte?
            (<Route exact path="/listeZZ" component={ListeZZ}/>)
            :
            (<Route exact path="/listeZZ" component={Accueil}/>)}
          {estConnecte?
            (<Route exact path="/mur" component={Mur}/>)
            :
            (<Route exact path="/mur" component={Accueil}/>)}
          <Route exact path="/" component={Accueil}/>
        </div>
      </div>
    );
  }

}

export default App;
