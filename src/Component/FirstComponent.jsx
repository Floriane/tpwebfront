import React, { Component } from 'react'


class FirstComponent extends Component
{

    constructor(props)
    {
        super(props)
    }

    render()
    {
        let texte = ""
        if(this.props.langue == "français")
        {
            texte = "Bonjour"
        }
        else
        {
            texte = "Hello"
        }

        return(
            <div>
                <button onClick={this.props.update}>{texte}</button>
            </div>
        )
    }
}

export default FirstComponent
