import React, { Component } from 'react'
import {Card} from 'react-materialize'

import UtilisateurCourant from "../Memoire/UtilisateurCourant"
import UtilisateurConnecte from "../Memoire/UtilisateurConnecte"


class ZZ extends Component
{

    allerPagePerso = () =>
    {
      if(UtilisateurConnecte.getNom() === this.props.utilisateur)
      {
        UtilisateurConnecte.setEstRegarde(true)
        this.props.history.push('/mur')
      }
      else {
        UtilisateurConnecte.setEstRegarde(false)
        UtilisateurCourant.update(this.props.utilisateur)
        this.props.history.push('/mur')
      }

    }

    render()
    {
        return(
          <div  style={utilisateurStyle}>
            <Card horizontal className="blue darken-1 white-text" onClick={this.allerPagePerso}>
              <p>{this.props.utilisateur}</p>
            </Card>
          </div>)
    }
}

const utilisateurStyle = {
  width : "200px",
  marginLeft : "2%",
  marginRight : "2%",}

export default ZZ
