import React, { Component } from 'react'
import {Card, Button, Icon} from 'react-materialize'
import axios from 'axios'

import UtilisateurConnecte from '../Memoire/UtilisateurConnecte'


class Message extends Component
{

    constructor(props)
    {
        super(props)
        this.state = {nbLike : this.props.nbLike, id : this.props.id, supprime : false}
    }

    clickLike = () =>
    {
      axios.post('http://localhost:5001/api/v1/clickLike', {id : this.state.id, nomClick : UtilisateurConnecte.getNom()}, {headers: {authorization : localStorage.getItem("token"), "Content-Type": "application/json"}})
            .then(retour =>{
                  this.setState({nbLike : retour.data.like})
                })
            .catch(err => console.log("Impossible de gerer like"))
    }

    clickDelete = () =>
    {
      axios.post('http://localhost:5001/api/v1/supprimerPost', {auteur :  UtilisateurConnecte.getNom(), id : this.state.id}, {headers: {authorization : localStorage.getItem("token"), "Content-Type": "application/json"}})
            .then(retour =>{
              UtilisateurConnecte.supprimerMessage(this.state.id)
              this.props.rechargement()
                })
            .catch(err => console.log("Impossible de supprimer le message"))
    }

    render()
    {
        return(
          <div  style={messageStyle}>
            <Card horizontal className="blue darken-1 white-text" actions={[<div style={buttonsStyle}>
                                                                                <Button onClick={this.clickLike}><Icon>grade</Icon>{this.state.nbLike}</Button>
                                                                                {
                                                                                  UtilisateurConnecte.getEstRegarde() && <Button className="red" onClick={this.clickDelete}><Icon>clear</Icon></Button>
                                                                                }
                                                                            </div>]} >
              <p>{this.props.message}</p>
            </Card>
          </div>)
    }
}

const messageStyle = {
  width : "80%",
  marginLeft : "2%",
  marginRight : "2%",
}

const buttonsStyle = {
  display : "flex",
  justifyContent : "space-between"
}

export default Message
