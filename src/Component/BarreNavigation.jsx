import React, { Component } from 'react'
import {Navbar, NavItem, Icon} from 'react-materialize'

import UtilisateurConnecte from "../Memoire/UtilisateurConnecte"

class BarreNavigation extends Component {

  deconnexion = () =>
  {
    localStorage.setItem("utilisateur","")
    localStorage.setItem("regarde","")
    localStorage.setItem("token","")
    this.props.history.push('/')
  }

  allerVoirZZ = () =>
  {
    this.props.history.push('/listeZZ')
  }

  allerMaPage = () =>
  {
    UtilisateurConnecte.setEstRegarde(true)
    this.props.history.push('/mur')
  }


  render() {
    return (
      <div>
        <Navbar className="light-blue darken-4" brand='InstaZZ' right style={{paddingLeft : '2%'}}>
          <NavItem onClick={this.allerMaPage}>{UtilisateurConnecte.getNom()}</NavItem>
          <NavItem onClick={this.allerVoirZZ}>Les ZZ</NavItem>
          <NavItem onClick={this.deconnexion}><Icon>power_settings_new</Icon></NavItem>
        </Navbar>
      </div>
    );
  }
}

export default BarreNavigation
