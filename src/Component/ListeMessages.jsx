import React, { Component } from 'react'

import Message from "./Message"


class ListeMessages extends Component
{

    render()
    {
        return(
          <div class="listeMessages" style={listeMessageStyle}>
            {this.props.listeMessages.map((post) => (<Message message={post.message} nbLike={post.like} id={post.id} key={post.id} rechargement={this.props.rechargement}/>))}
          </div>)
    }
}

const listeMessageStyle =
{
  width : "100%",
  display : "flex",
  flexDirection : "column",
  alignItems : "center",
}

export default ListeMessages
