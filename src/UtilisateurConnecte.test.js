import UtilisateurConnecte from './Memoire/UtilisateurConnecte'


// Utilisateur Connecte
// pour l'ensemble des tests qui suivent
UtilisateurConnecte.init("Bob")


it('Test init UtilisateurConnecte Nom', () => {
  expect(UtilisateurConnecte.getNom()).toBe("Bob")
})


it('Test init UtilisateurConnecte Messages', () => {
  expect(UtilisateurConnecte.getMessages()).toEqual([])
})


it('Test init UtilisateurConnecte estRegarde', () => {
  expect(UtilisateurConnecte.getEstRegarde()).toBe(true)
})


it('Test setEstRegarde UtilisateurConnecte', () => {
  UtilisateurConnecte.setEstRegarde(false)
  expect(UtilisateurConnecte.getEstRegarde()).toBe(false)
})

it('Test ajouterMessage UtilisateurConnecte nbMessage < 5', () => {
  UtilisateurConnecte.ajouterMessage({message :"Bonjour1", like : 0, _id : 1})
  expect(UtilisateurConnecte.getMessages()).toEqual([{message :"Bonjour1", like : 0, id : 1}])
})

it('Test ajouterMessage UtilisateurConnecte nbMessage > 5', () => {
  UtilisateurConnecte.ajouterMessage({message :"Bonjour2", like : 0, _id : 2})
  UtilisateurConnecte.ajouterMessage({message :"Bonjour3", like : 0, _id : 3})
  UtilisateurConnecte.ajouterMessage({message :"Bonjour4", like : 0, _id : 4})
  UtilisateurConnecte.ajouterMessage({message :"Bonjour5", like : 0, _id : 5})
  expect(UtilisateurConnecte.getMessages()).toEqual([{message : "Bonjour5", like : 0, id : 5},{message : "Bonjour4", like :0 , id : 4},{message : "Bonjour3", like : 0, id : 3},{message : "Bonjour2", like : 0, id : 2},{message : "Bonjour1", like : 0, id : 1}])
})


it("Test clearMessage UtilisateurConnecte", () => {
  UtilisateurConnecte.clearMessages()
    expect(UtilisateurConnecte.getMessages()).toEqual([])
})

it("Test clear UtilisateurConnecte Messages", () => {
  UtilisateurConnecte.ajouterMessage("test clear")
  UtilisateurConnecte.clear()
  expect(UtilisateurConnecte.getMessages()).toEqual([])
})

it("Test clear UtilisateurConnecte Nom", () => {
  UtilisateurConnecte.init("Bob")
  UtilisateurConnecte.clear()
  expect(UtilisateurConnecte.getNom()).toBe("")
})
